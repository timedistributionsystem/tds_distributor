package com.example.tng0715.distribution_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tng0715 on 8/27/2015.
 */
public class UserBundleAdapter extends ArrayAdapter {
    List list = new ArrayList<>();

    public UserBundleAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler {
        TextView tv_userBundle;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_user_detail_layout,parent,false);
            handler = new DataHandler();
            handler.tv_userBundle = (TextView) row.findViewById(R.id.tv_userBundle);
            row.setTag(handler);
        } else {
            handler = (DataHandler) row.getTag();
        }
        UserBundleDataProvider dataProvider;
        dataProvider = (UserBundleDataProvider) this.getItem(position);
        handler.tv_userBundle.setText(dataProvider.getUser_bundle());

        return row;
    }
}
