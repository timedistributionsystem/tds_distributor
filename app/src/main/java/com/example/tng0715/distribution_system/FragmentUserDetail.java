package com.example.tng0715.distribution_system;


import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Spinner;

public class FragmentUserDetail extends Fragment {
    String TAG = "LoginTesting";

    ListView lv_userBundle;
    UserBundleAdapter adapter_userBundle;

    Spinner sp_fmDate;
    SpinnerAdapter adapter_fmDate;
    private String fmDate;

    Spinner sp_countryCode;
    SpinnerAdapter adapter_countryCode;
    private String countryCode;

    Spinner sp_productCode;
    SpinnerAdapter adapter_productCode;
    private String productCode;

    Spinner sp_roadID;
    SpinnerAdapter adapter_roadID;
    private String roadID;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_detail, container, false);

        lv_userBundle = (ListView) v.findViewById(R.id.listView_userDetail);
        adapter_userBundle = new UserBundleAdapter(v.getContext(), R.layout.row_user_detail_layout);
        lv_userBundle.setAdapter(adapter_userBundle);
        adapter_userBundle.clear();
        adapter_userBundle.notifyDataSetChanged();

        sp_fmDate = (Spinner) v.findViewById(R.id.sp_fmDate);
        adapter_fmDate = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_fmDate.setAdapter(adapter_fmDate);
        adapter_fmDate.clear();
        adapter_fmDate.notifyDataSetChanged();
        sp_fmDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SpinnerDataProvider spinnerDataProvider = (SpinnerDataProvider)adapterView.getItemAtPosition(i);
                setFmDate(spinnerDataProvider.getSpinnerText());
//                Log.d("UserDetailTesting",spinnerDataProvider.getSpinnerText());
                adapter_userBundle = new UserBundleAdapter(getContext(), R.layout.row_user_detail_layout);
                lv_userBundle.setAdapter(adapter_userBundle);
                adapter_userBundle.clear();
                adapter_userBundle.notifyDataSetChanged();

                new FragmentUserDetail.getUserBundleTask().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_countryCode = (Spinner) v.findViewById(R.id.sp_countryCode);
        adapter_countryCode = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_countryCode.setAdapter(adapter_countryCode);
        adapter_countryCode.clear();
        adapter_countryCode.notifyDataSetChanged();

        sp_productCode = (Spinner) v.findViewById(R.id.sp_productCode);
        adapter_productCode = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_productCode.setAdapter(adapter_productCode);
        adapter_productCode.clear();
        adapter_productCode.notifyDataSetChanged();

        sp_roadID = (Spinner) v.findViewById(R.id.sp_roadID);
        adapter_roadID = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_roadID.setAdapter(adapter_roadID);
        adapter_roadID.clear();
        adapter_roadID.notifyDataSetChanged();

        new getUserDetailTask().execute();

        return v;
    }

    private class getUserDetailTask extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
            String url_select = "http://t-end.net/tntommy/distribution/json/getUserDetail.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                is = httpEntity.getContent();
            } catch (Exception e) {
                Log.e("LogResult", "Error in http connection " + e.toString());
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("LogResult", "Error converting result " + e.toString());
            }

            return null;
        }

        protected void onPostExecute(Void v) {
            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = json.getBoolean("server_status");
                boolean user_login = json.getBoolean("user_login");

                if (server_status) {
                    try {
                        JSONArray fm_date = json.getJSONArray("fm_date");
                        setFmDate(fm_date.get(0).toString());
                        for (int i = 0; i < fm_date.length(); i++) {
                            Log.d("LogResult", "fm_date:" + fm_date.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(fm_date.get(i).toString());
                            adapter_fmDate.add(dataProvider);
                        }
                        JSONArray road_ID = json.getJSONArray("road_ID");
                        setRoadID(road_ID.get(0).toString());
                        for (int i = 0; i < road_ID.length(); i++) {
                            Log.d("LogResult", "road_ID:" + road_ID.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(road_ID.get(i).toString());
                            adapter_roadID.add(dataProvider);
                        }
                        JSONArray country_code = json.getJSONArray("country_code");
                        setCountryCode(country_code.get(0).toString());
                        for (int i = 0; i < country_code.length(); i++) {
                            Log.d("LogResult", "country_code:" + country_code.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(country_code.get(i).toString());
                            adapter_countryCode.add(dataProvider);
                        }
                        JSONArray product_code = json.getJSONArray("product_code");
                        setProductCode(product_code.get(0).toString());
                        for (int i = 0; i < product_code.length(); i++) {
                            Log.d("LogResult", "product_code:" + product_code.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(product_code.get(i).toString());
                            adapter_productCode.add(dataProvider);
                        }

                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {

            }
        }
    }

    private class getUserBundleTask extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
//            String url_select = "http://www.timeasiasubs.com/timeasia/phpJson/checkOwnBundle.php";
            String url_select = "http://t-end.net/tntommy/distribution/json/checkOwnBundle.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));
            param.add(new BasicNameValuePair("fmDate", getFmDate()));
            param.add(new BasicNameValuePair("countryCode", getCountryCode()));
            param.add(new BasicNameValuePair("productCode", getProductCode()));
            param.add(new BasicNameValuePair("roadID", getRoadID()));

//            Log.d("UserDetailTesting",getFmDate());
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e(TAG, "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {
            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");

                if (server_status) {
                    try {
                        JSONObject user_information = json.getJSONObject("user_information");

                        try {
                            JSONArray user_bundle = json.getJSONArray("user_bundle");
                            for (int i = 0; i < user_bundle.length(); i++) {
                                JSONObject user_bundle_object = user_bundle.getJSONObject(i);
                                String lbl_bundle_no = user_bundle_object.getString("lbl_bundle_no");

                                UserBundleDataProvider dataProvider = new UserBundleDataProvider(lbl_bundle_no);
                                adapter_userBundle.add(dataProvider);
                            }

                        } catch (Exception e) {

                        }

                    } catch (Exception e) {

                    }

                } else {

                }

//                Toast.makeText(getActivity(), "Record(s) found.", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
        }
    }

    private void setFmDate(String fmDate){
        this.fmDate = fmDate;
    }

    public String getFmDate(){
        return this.fmDate;
    }

    private void setCountryCode(String countryCode){
        this.countryCode = countryCode;
    }

    public String getCountryCode(){
        return this.countryCode;
    }

    private void setProductCode(String productCode){
        this.productCode = productCode;
    }

    public String getProductCode(){
        return this.productCode;
    }

    private void setRoadID(String roadID){
        this.roadID = roadID;
    }

    public String getRoadID(){
        return this.roadID;
    }

}
