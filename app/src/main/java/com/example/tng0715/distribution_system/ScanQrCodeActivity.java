package com.example.tng0715.distribution_system;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;


public class ScanQrCodeActivity extends Activity{
    String url_result, userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userID = extras.getString("userID");
        }

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setPrompt("");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                url_result = result.getContents();
                new task().execute();
                finish();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private class task extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
            String url_select;
            url_select = url_result;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));

            try {
                HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

                DefaultHttpClient client = new DefaultHttpClient();

                SchemeRegistry registry = new SchemeRegistry();
                SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
                socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
                registry.register(new Scheme("https", socketFactory, 443));
                SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(),registry);
                httpClient = new DefaultHttpClient(mgr,client.getParams());

                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Incorrect QR code", Toast.LENGTH_SHORT).show();
                    }
                });
                task.this.cancel(true);
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Incorrect QR code", Toast.LENGTH_SHORT).show();
                    }
                });
                task.this.cancel(true);
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                Boolean user_login = (Boolean) json.get("user_login");
                Boolean bundle_status = (Boolean) json.get("bundle_status");
                String bundle_ID = json.get("bundle_ID") + "";
                boolean server_status = (boolean) json.get("server_status");
                //_("Json value :" + bundle_ID);
                if (user_login) {
                    if (!(bundle_ID.equalsIgnoreCase(null + ""))) {
                        if (bundle_status) {
                            if (server_status) {
                                Toast.makeText(ScanQrCodeActivity.this, "Check In Successfully", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(ScanQrCodeActivity.this, "Bundle Already Checked In With This User ID.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(ScanQrCodeActivity.this, "You Don't Have The Access Right To Check In This Bundle!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ScanQrCodeActivity.this, "There Is Something Wrong With The Qr Code. Please Re-scan The Label or Manually Input The ID.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (!server_status) {
                        Toast.makeText(ScanQrCodeActivity.this, "You Must Log In To Check In The Bundle.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ScanQrCodeActivity.this, "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
        }
    }

}
