package com.example.tng0715.distribution_system;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.support.v4.app.Fragment;


/**
 * Created by hcwong0601 on 8/18/2015.
 */


public class FragmentDeliveryStatus extends Fragment implements View.OnClickListener {

    ListView listView;
    EditText editText;
    Button button;
    String bundleID;

    //Testing Customize List View
    int join = R.drawable.ic_join;
    int empty = R.drawable.ic_empty;
    int bundle = R.drawable.ic_box;
    int error = R.drawable.ic_error;
    String[] dist_time;
    String[] dist_location;
    DeliveryStatusAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delivery_status_layout, container, false);
        listView = (ListView) v.findViewById(R.id.listView);
        editText = (EditText) v.findViewById(R.id.editText);
        button = (Button) v.findViewById(R.id.button);

        //Testing Customize List View

        adapter = new DeliveryStatusAdapter(v.getContext(), R.layout.row_delivery_status_layout);
        listView.setAdapter(adapter);

        /*for (String name : dist_time) {
            if (i != dist_time.length - 1) {
                DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, name, dist_location[i], empty);
                adapter_distributionMap.add(dataProvider);
                i++;
            } else {
                DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, name, dist_location[i], error);
                adapter_distributionMap.add(dataProvider);
                i++;
            }
        }*/


        button.setOnClickListener(this);

        return v;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    private class task extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(getActivity());
        InputStream is = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Fetching data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    task.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            //String url_select = "http://www.timeasiasubs.com/timeasia/phpJson/deliveryStatus.php";
            String url_select = "http://t-end.net/tntommy/distribution/json/deliveryStatus.php";


            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));
            param.add(new BasicNameValuePair("bundleID", bundleID));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.d("LoginTesting",result);

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                boolean user_login = (boolean) json.get("user_login");
                boolean bundle_status = (boolean) json.get("bundle_status");
                boolean dist_report_status = (boolean) json.get("dist_report_status");
                String bundle_ID = (String) json.get("bundle_ID");

                if (user_login) {
                    if (bundle_status) {
                        try {
                            JSONArray delivery_status = json.getJSONArray("delivery_status");
                            JSONArray user_distribution_map = json.getJSONArray("user_distribution_map");
                            //Search The Array And Put Them Into The Adapter
                            int j = 0;
                            for (int i = 0; i < user_distribution_map.length(); i++) {
                                JSONObject user_distribution_map_object = user_distribution_map.getJSONObject(i);

                                String dm_checkpoint = user_distribution_map_object.getString("dm_checkpoint");
                                String dm_checkpoint_desc = user_distribution_map_object.getString("dm_checkpoint_desc");

                                if (i<delivery_status.length()) {
                                    JSONObject delivery_status_object = delivery_status.getJSONObject(i);

                                    //Get The Array Data
                                    String ds_did = delivery_status_object.getString("ds_did");
                                    String ds_timestamp = delivery_status_object.getString("ds_timestamp");

                                    String msg = ds_did + " : Bundle Arrived At " + dm_checkpoint_desc;
                                    if (i != delivery_status.length() - 1) {
                                        DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, ds_timestamp, msg , empty);
                                        adapter.add(dataProvider);
                                    } else {
                                        DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, ds_timestamp, msg , bundle);
                                        adapter.add(dataProvider);
                                    }
                                } else {
                                    String title = "Checkpoint " + dm_checkpoint;
                                    String msg = dm_checkpoint_desc;
                                    DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, title, msg , empty);
                                    adapter.add(dataProvider);
                                }

                                if (dist_report_status && i<delivery_status.length()) {
                                    JSONObject delivery_status_object = delivery_status.getJSONObject(i);
                                    //Get The Array Data
                                    String ds_did = delivery_status_object.getString("ds_did");
                                    JSONArray dist_report = json.getJSONArray("dist_report");
                                    String dr_did;
                                    try {
                                        JSONObject dist_report_object = dist_report.getJSONObject(j);
                                        dr_did = dist_report_object.getString("dr_did");
                                    } catch (Exception ex) {
                                        dr_did = null;
                                    }
                                    while (ds_did.equals(dr_did)) {
                                        try {
                                            JSONObject dist_report_object = dist_report.getJSONObject(j);
                                            dr_did = dist_report_object.getString("dr_did");
                                            String dr_message = dist_report_object.getString("dr_message");
                                            String dr_timestamp = dist_report_object.getString("dr_timestamp");
                                            DeliveryStatusDataProvider dataProvider = new DeliveryStatusDataProvider(join, dr_timestamp, dr_did + " : " + dr_message, error);
                                            adapter.add(dataProvider);

                                            j++;
                                            dist_report_object = dist_report.getJSONObject(j);
                                            dr_did = dist_report_object.getString("dr_did");
                                        } catch (Exception ex) {
                                            break;
                                        }
                                    }
                                }

                            }
                        } catch (Exception e){
                            Log.e("asd",e.toString());
                            Toast.makeText(getActivity(), "This Bundle Does Not Have Any Delivery Status!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "You Don't Have The Access Right To Search This Bundle!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (!server_status) {
                        Toast.makeText(getActivity(), "You Must Log In To Search This Bundle.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                    }
                }

//                Toast.makeText(getActivity(), "Record(s) found.", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
            this.progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context
                .INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(),0);
        bundleID = (editText.getText() + "").toLowerCase();
        adapter = new DeliveryStatusAdapter(v.getContext(), R.layout.row_delivery_status_layout);
        listView.setAdapter(adapter);
        adapter.clear();
        adapter.notifyDataSetChanged();
        new task().execute();
    }
}


