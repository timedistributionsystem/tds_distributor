package com.example.tng0715.distribution_system;

/**
 * Created by tng0715 on 8/25/2015.
 */
public class DistributionMapDataProvider {
    private String check_point_number;
    private String checkpoint_desc;

    public DistributionMapDataProvider(String check_point_number,String checkpoint_desc){
        this.setCheck_point_number(check_point_number);
        this.setCheckpoint_desc(checkpoint_desc);
    }

    public String getCheck_point_number() {
        return check_point_number;
    }

    public void setCheck_point_number(String check_point_number) {
        this.check_point_number = check_point_number;
    }

    public String getCheckpoint_desc() { return checkpoint_desc; }

    public void setCheckpoint_desc(String checkpoint_desc) {this.checkpoint_desc=checkpoint_desc;}
}
