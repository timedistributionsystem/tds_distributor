package com.example.tng0715.distribution_system;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by hcwong0601 on 8/19/2015.
 */
public class FragmentReport extends Fragment implements View.OnClickListener {
    EditText editText_report, editText_reportcontent;
    Button button_report;
    String bundleID, distReportMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report_layout, container, false);
        editText_report = (EditText) v.findViewById(R.id.editText_report);
        editText_reportcontent = (EditText) v.findViewById(R.id.editText_reportcontent);

        TextInputLayout textInputLayout = (TextInputLayout) v.findViewById(R.id.inputLayout);

        button_report = (Button) v.findViewById(R.id.button_report);

        button_report.setOnClickListener(this);

        return v;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context
            .INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText_report.getWindowToken(),0);
        inputMethodManager.hideSoftInputFromWindow(editText_reportcontent.getWindowToken(),0);

        bundleID = (editText_report.getText() + "").toLowerCase();
        distReportMessage = editText_reportcontent.getText() + "";
        if (bundleID.equals("")) {
            Toast.makeText(getActivity(), "Please Enter The Bundle ID.", Toast.LENGTH_SHORT).show();
        }
        if (distReportMessage.equals("")) {
            Toast.makeText(getActivity(), "Please Enter The Report Message.", Toast.LENGTH_SHORT).show();
        }
        if (!bundleID.equals("") && !distReportMessage.equals(""))
            new task().execute();
    }

    private class task extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(getActivity());
        InputStream is = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Fetching data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    task.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
//            String url_select = "http://www.timeasiasubs.com/timeasia/phpJson/distributorReport.php";
            String url_select = "http://t-end.net/tntommy/distribution/json/distributorReport.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));
            param.add(new BasicNameValuePair("bundleID", bundleID));
            param.add(new BasicNameValuePair("distReportMessage", distReportMessage));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                boolean user_login = (boolean) json.get("user_login");
                boolean bundle_status = (boolean) json.get("bundle_status");
                boolean report_status = (boolean) json.get("report_status");

                if (user_login) {
                    if (bundle_status) {
                        if (report_status) {
                            Toast.makeText(getActivity(), "Report Success.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "You Have Already Reported This Bundle ID or Label ID.", Toast.LENGTH_LONG).show();
                        }
                        //Search The Array And Put Them Into The Adapter
                    } else {
                        Toast.makeText(getActivity(), "You Don't Have The Access Right To Report This Bundle!", Toast.LENGTH_LONG).show();
//                        Toast.makeText(getActivity(), bundle_ID +" vs "+ bundleID, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (!server_status) {
                        Toast.makeText(getActivity(), "You Must Log In To Report This Bundle.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                    }
                }

                this.progressDialog.dismiss();
//                Toast.makeText(getActivity(), "Record(s) found.", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
        }
    }
}
