package com.example.tng0715.distribution_system;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class LoginActivity extends Activity {

    Context c;

    EditText etUserID;
    EditText etEmail;
    String userID;
    String email;

    boolean checkLogin;

    ImageButton ibLogin;

    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = this;

        setContentView(R.layout.activity_login);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        etUserID = (EditText) findViewById(R.id.etUserID);
        etEmail = (EditText) findViewById(R.id.etEmail);
        checkLogin = false;

        etUserID.setText("314");
        etEmail.setText("314@test.com");

        final Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No network connection.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (!isInternetConnected())
            snackbar.show();

        ibLogin = (ImageButton) findViewById(R.id.ibLogin);
        ibLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(etEmail.getWindowToken(),0);
                inputMethodManager.hideSoftInputFromWindow(etUserID.getWindowToken(),0);

                userID = etUserID.getText() + "";
                email = etEmail.getText() + "";

                if (userID.length() == 0 || email.length() == 0) {
                    Toast.makeText(c, "Please fill in userID and email", Toast.LENGTH_SHORT).show();
                    return;
                }

                task t = new task();
                t.execute();
            }
        });
    }

    boolean isInternetConnected() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context
                    .CONNECTIVITY_SERVICE);

            if(connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected()){
                return true;
            }
            else{
                return  false;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    class task extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
//            String url_select = "http://www.timeasiasubs.com/timeasia/phpJson/checkLogin.php";
            String url_select = "http://t-end.net/tntommy/distribution/json/checkLogin.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));
            param.add(new BasicNameValuePair("userEmail", email));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                if (server_status) {
                    if (!checkLogin) {
                        Intent it = new Intent();
                        it.setClass(LoginActivity.this, HomeActivity.class);
                        it.putExtra("userID", userID);
                        startActivity(it);
                        checkLogin = true;
                    }
                    finish();
                } else {
                    Toast.makeText(c, "Please Check Your UserID and Password", Toast
                            .LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                // TODO: handle exception
            }
        }
    }
}
