package com.example.tng0715.distribution_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends ArrayAdapter {
    List list = new ArrayList<>();

    public SpinnerAdapter(Context context, int resource, int textViewId){
        super(context,resource,textViewId);
    }

    static class DataHandler{
        TextView tv_spinnerText;
    }

    @Override
    public void add(Object object){
        super.add(object);
        list.add(object);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent){
        View row;
        row = convertView;
        DataHandler handler;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_spinner,parent,false);
            handler = new DataHandler();
            handler.tv_spinnerText = (TextView) row.findViewById(R.id.tv_spinnerText);

            row.setTag(handler);
        } else {
            handler = (DataHandler) convertView.getTag();
        }
        SpinnerDataProvider dataProvider;
        dataProvider = (SpinnerDataProvider) this.getItem(position);
        handler.tv_spinnerText.setText(dataProvider.getSpinnerText());

        return row;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row;
        row = convertView;
        DataHandler handler;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_spinner,parent,false);
            handler = new DataHandler();
            handler.tv_spinnerText = (TextView) row.findViewById(R.id.tv_spinnerText);
            row.setTag(handler);
        } else {
            handler = (DataHandler) row.getTag();
        }
        SpinnerDataProvider dataProvider;
        dataProvider = (SpinnerDataProvider) this.getItem(position);
        handler.tv_spinnerText.setText(dataProvider.getSpinnerText());

        return row;
    }
}
