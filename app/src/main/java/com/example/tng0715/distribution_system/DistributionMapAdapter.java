package com.example.tng0715.distribution_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tng0715 on 8/25/2015.
 */
public class DistributionMapAdapter extends ArrayAdapter {
    List list = new ArrayList<>();

    public DistributionMapAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler {
        TextView textView_checkPoint;
        TextView textView_checkpoint_desc;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_distribution_map_layout, parent, false);
            handler = new DataHandler();
            handler.textView_checkPoint = (TextView) row.findViewById(R.id.textView_checkPoint);
            handler.textView_checkpoint_desc = (TextView) row.findViewById(R.id.textView_checkpoint_desc);
            row.setTag(handler);
        } else {
            handler = (DataHandler) row.getTag();
        }
        DistributionMapDataProvider dataProvider;
        dataProvider =(DistributionMapDataProvider) this.getItem(position);
        handler.textView_checkPoint.setText(dataProvider.getCheck_point_number());
        handler.textView_checkpoint_desc.setText(dataProvider.getCheckpoint_desc());

        return row;
    }
}
