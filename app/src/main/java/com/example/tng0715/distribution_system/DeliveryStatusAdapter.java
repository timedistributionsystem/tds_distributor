package com.example.tng0715.distribution_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tng0715 on 8/21/2015.
 */
public class DeliveryStatusAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public DeliveryStatusAdapter(Context context, int resource) {
        super(context, resource);
    }

    static class DataHandler {
        ImageView picture;
        TextView name;
        TextView password;
        ImageView bundle;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_delivery_status_layout, parent, false);
            handler = new DataHandler();
            handler.picture = (ImageView) row.findViewById(R.id.dist_arrow);
            handler.name = (TextView) row.findViewById(R.id.dist_time);
            handler.password = (TextView) row.findViewById(R.id.dist_location);
            handler.bundle = (ImageView) row.findViewById(R.id.dist_bundle);
            row.setTag(handler);
        } else {
            handler = (DataHandler) row.getTag();
        }
        DeliveryStatusDataProvider dataProvider;
        dataProvider = (DeliveryStatusDataProvider) this.getItem(position);
        handler.picture.setImageResource(dataProvider.getDist_arrow());
        handler.name.setText(dataProvider.getDist_time());
        handler.password.setText(dataProvider.getDist_location());
        handler.bundle.setImageResource(dataProvider.getDist_bundle());

        return row;
    }
}
