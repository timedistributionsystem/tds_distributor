package com.example.tng0715.distribution_system;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.support.v4.app.Fragment;


public class FragmentDistributionMap extends Fragment {
    ListView lv_distributionMap;
    DistributionMapAdapter adapter_distributionMap;

    Spinner sp_fmDate;
    SpinnerAdapter adapter_fmDate;
    private String fmDate;

    Spinner sp_roadID;
    SpinnerAdapter adapter_roadID;
    private String roadID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_distribution_map_layout, container, false);

        lv_distributionMap = (ListView) v.findViewById(R.id.listView_distMap);
        adapter_distributionMap = new DistributionMapAdapter(v.getContext(), R.layout.row_distribution_map_layout);
        lv_distributionMap.setAdapter(adapter_distributionMap);
        adapter_distributionMap.clear();
        adapter_distributionMap.notifyDataSetChanged();

        sp_fmDate = (Spinner) v.findViewById(R.id.sp_fmDate);
        adapter_fmDate = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_fmDate.setAdapter(adapter_fmDate);
        adapter_fmDate.clear();
        adapter_fmDate.notifyDataSetChanged();

        sp_roadID = (Spinner) v.findViewById(R.id.sp_roadID);
        adapter_roadID = new SpinnerAdapter(v.getContext(), R.layout.row_spinner, R.id.tv_spinnerText);
        sp_roadID.setAdapter(adapter_roadID);
        adapter_roadID.clear();
        adapter_roadID.notifyDataSetChanged();

        new getUserDetailTask().execute();

        return v;
    }

    private class getUserDetailTask extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
            String url_select = "http://t-end.net/tntommy/distribution/json/getUserDetail.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                is = httpEntity.getContent();
            } catch (Exception e) {
                Log.e("LogResult", "Error in http connection " + e.toString());
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("LogResult", "Error converting result " + e.toString());
            }

            return null;
        }

        protected void onPostExecute(Void v) {
            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = json.getBoolean("server_status");
                boolean user_login = json.getBoolean("user_login");

                if (server_status) {
                    try {
                        JSONArray fm_date = json.getJSONArray("fm_date");
                        setFmDate(fm_date.get(0).toString());
                        for (int i = 0; i < fm_date.length(); i++) {
                            Log.d("LogResult", "fm_date:" + fm_date.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(fm_date.get(i).toString());
                            adapter_fmDate.add(dataProvider);
                        }
                        JSONArray road_ID = json.getJSONArray("road_ID");
                        setRoadID(road_ID.get(0).toString());
                        for (int i = 0; i < road_ID.length(); i++) {
                            Log.d("LogResult", "road_ID:" + road_ID.get(i).toString());
                            SpinnerDataProvider dataProvider = new SpinnerDataProvider(road_ID.get(i).toString());
                            adapter_roadID.add(dataProvider);
                        }

                        new getDistributiomMapTask().execute();

                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {

            }
        }
    }

    private class getDistributiomMapTask extends AsyncTask<String, String, Void> {
        InputStream is = null;
        String result = "";

        @Override
        protected Void doInBackground(String... params) {
            //String url_select = "http://www.timeasiasubs.com/timeasia/phpJson/distributionMap.php";
            String url_select = "http://t-end.net/tntommy/distribution/json/distributionMap.php";

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            HomeActivity homeActivity = (HomeActivity) getActivity();
            String userID = homeActivity.getMyData();

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));
            param.add(new BasicNameValuePair("fmDate", getFmDate()));
            param.add(new BasicNameValuePair("roadID", getRoadID()));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                boolean server_status = (boolean) json.get("server_status");
                boolean user_login = (boolean) json.get("user_login");

                if (user_login) {
                    try {
                        JSONArray user_distribution_map = json.getJSONArray("user_distribution_map");
                        for (int i = 0; i < user_distribution_map.length(); i++) {
                            JSONObject user_distribution_map_object = user_distribution_map.getJSONObject(i);

                            String dm_checkpoint = user_distribution_map_object.getString("dm_checkpoint");
                            String dm_checkpoint_desc = user_distribution_map_object.getString("dm_checkpoint_desc");

                            DistributionMapDataProvider dataProvider = new DistributionMapDataProvider(dm_checkpoint, dm_checkpoint_desc);
                            adapter_distributionMap.add(dataProvider);
                        }
                    } catch (Exception e) {

                    }

                } else {
                    if (!server_status) {
                        Toast.makeText(getActivity(), "You Must Log In To Report This Bundle.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                    }
                }

//                Toast.makeText(getActivity(), "Record(s) found.", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
        }
    }

    private void setFmDate(String fmDate){
        this.fmDate = fmDate;
    }

    public String getFmDate(){
        return this.fmDate;
    }

    private void setRoadID(String roadID){
        this.roadID = roadID;
    }

    public String getRoadID(){
        return this.roadID;
    }
}
