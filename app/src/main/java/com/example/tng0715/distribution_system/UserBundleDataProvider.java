package com.example.tng0715.distribution_system;

/**
 * Created by tng0715 on 8/27/2015.
 */
public class UserBundleDataProvider {
    private String user_bundle;

    public UserBundleDataProvider(String user_bundle){
        this.setUser_bundle(user_bundle);
    }

    public String getUser_bundle() {
        return user_bundle;
    }

    public void setUser_bundle(String user_bundle) {
        this.user_bundle = user_bundle;
    }
}
