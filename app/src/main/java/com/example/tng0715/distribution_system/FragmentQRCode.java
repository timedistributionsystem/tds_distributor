package com.example.tng0715.distribution_system;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import android.support.v4.app.Fragment;

/**
 * Created by hcwong0601 on 8/18/2015.
 */
public class FragmentQRCode extends Fragment {
    String url_result, bundleID, userID;
    Button btn_CheckIn, btn_scanQrCode;
    EditText et_ID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_qrcode_layout, container, false);

        HomeActivity homeActivity = (HomeActivity) getActivity();
        userID = homeActivity.getMyData();

        btn_CheckIn = (Button) v.findViewById(R.id.btn_CheckIn);
        btn_scanQrCode = (Button) v.findViewById(R.id.btn_scanQrCode);
        et_ID = (EditText) v.findViewById(R.id.et_ID);

        btn_scanQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(et_ID.getWindowToken(),0);
                Intent i = new Intent();
                i.setClass(v.getContext(), ScanQrCodeActivity.class);
                i.putExtra("userID",userID);
                startActivity(i);
            }
        });

        btn_CheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(et_ID.getWindowToken(),0);
                bundleID = (et_ID.getText() + "").toLowerCase();
                new task().execute();
            }
        });

        return v;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    private class task extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(getActivity());
        InputStream is = null;
        String result = "";

        protected void onPreExecute() {
            progressDialog.setMessage("Fetching data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    task.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            String url_select;
//            url_select = "https://www.timeasiasubs.com/timeasia/phpJson/scanQrCode.php?bundleID=" + bundleID;
            url_select = "http://t-end.net/tntommy/distribution/json/scanQrCode.php?bundleID=" + bundleID;

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url_select);

            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userID", userID));

            try {
                HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

                DefaultHttpClient client = new DefaultHttpClient();

                SchemeRegistry registry = new SchemeRegistry();
                SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
                socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
                registry.register(new Scheme("https", socketFactory, 443));
                SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(),registry);
                httpClient = new DefaultHttpClient(mgr,client.getParams());

                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

                httpPost.setEntity(new UrlEncodedFormEntity(param));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();

            } catch (Exception e) {

                Log.e("LoginTesting", "Error in http connection " + e.toString());
                //Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error converting result " + e.toString());
            }

            return null;

        }

        protected void onPostExecute(Void v) {

            // ambil data dari Json database

            try {
                JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
                Boolean user_login = (Boolean) json.get("user_login");
                Boolean bundle_status = (Boolean) json.get("bundle_status");
                String bundle_ID = json.get("bundle_ID") + "";
                boolean server_status = (boolean) json.get("server_status");
                _("Json value :" + bundle_ID);
                if (user_login) {
                    if (!(bundle_ID.equalsIgnoreCase(null + ""))) {
                        if (bundle_status) {
                            if (server_status) {
                                Toast.makeText(getActivity(), "Check In Successfully", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Bundle Already Checked In With This User ID.", Toast.LENGTH_LONG).show();
                                _("Bundle Already Checked In With This User ID.");
                            }
                        } else {
                            Toast.makeText(getActivity(), "You Don't Have The Access Right To Check In This Bundle!", Toast.LENGTH_LONG).show();
                            _("You Don't The Access Right To Check In This Bundle!");
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Re-scan The Label or Manually Input The ID.", Toast.LENGTH_LONG).show();
                        _("Please Check Your Input And Input The ID Again.");
                    }
                } else {
                    if (!server_status) {
                        Toast.makeText(getActivity(), "You Must Log In To Check In The Bundle.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Unable To Connect To The Database", Toast.LENGTH_LONG).show();
                    }
                }

                this.progressDialog.dismiss();

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("LoginTesting", "Error parsing data " + e.toString());
            }
        }
    }

    private void _(String s) {
        Log.d("LoginTesting", "LoginActivity" + "########" + s);
    }
}
