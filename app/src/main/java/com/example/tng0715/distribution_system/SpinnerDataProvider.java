package com.example.tng0715.distribution_system;

/**
 * Created by NgFamily on 21/12/2016.
 */

public class SpinnerDataProvider {
    private String spinnerText;

    public SpinnerDataProvider(String spinnerText){
        this.setSpinnerText(spinnerText);
    }

    public String getSpinnerText(){
        return spinnerText;
    }

    public void setSpinnerText(String spinnerText){
        this.spinnerText = spinnerText;
    }
}
