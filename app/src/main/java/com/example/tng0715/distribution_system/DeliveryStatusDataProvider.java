package com.example.tng0715.distribution_system;

/**
 * Created by tng0715 on 8/21/2015.
 */
public class DeliveryStatusDataProvider {
    private int dist_arrow;
    private String dist_time;
    private String dist_location;
    private int dist_bundle;

    public DeliveryStatusDataProvider(int dist_arrow, String dist_time, String dist_location, int dist_bundle) {
        this.setDist_arrow(dist_arrow);
        this.setDist_time(dist_time);
        this.setDist_location(dist_location);
        this.setDist_bundle(dist_bundle);
    }

    public String getDist_location() {
        return dist_location;
    }

    public void setDist_location(String dist_location) {
        this.dist_location = dist_location;
    }

    public String getDist_time() {
        return dist_time;
    }

    public void setDist_time(String dist_time) {
        this.dist_time = dist_time;
    }

    public int getDist_arrow() {
        return dist_arrow;
    }

    public void setDist_arrow(int dist_arrow) {
        this.dist_arrow = dist_arrow;
    }

    public int getDist_bundle() {
        return dist_bundle;
    }

    public void setDist_bundle(int dist_bundle) {
        this.dist_bundle = dist_bundle;
    }
}
